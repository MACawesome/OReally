from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
import requests
import urllib
import os
import pyperclip
import pyautogui
import re
import sys

def get_oreilly_book(booktype, name):
    # Get a booktype and a name
    # makes the url for the oreilly page.
    # Starts a firefox instance and goes to the page
    # Fill form with premade instructions
    # Goes to download page, controls cursor to
    # copy epub download link.
    # Call bash commands to download book in library
    name = re.sub(' ', '-', name)
    download_name = re.sub('-', '_', name)
    full_path = ('http://www.oreilly.com/' + booktype + '/free/' +
                 name + '.csp')

    driver = webdriver.Firefox()
    driver.get(full_path)
    first_name = driver.find_element_by_name('first')
    first_name.send_keys("malus")
    last_name = driver.find_element_by_name("last")
    last_name.send_keys("maleficarum")
    email = driver.find_element_by_name("email")
    email.send_keys("malus_maleficarum@hotmail.com")
    newsletter_click = driver.find_element_by_name("newsletter")
    newsletter_click.send_keys(Keys.RETURN)

    positions = None
    while positions is None:
        positions = pyautogui.locateCenterOnScreen('epub_symbol.png')
        
    positions = pyautogui.locateCenterOnScreen('epub_symbol.png')
    pyautogui.moveTo(positions[0], positions[1])
    pyautogui.click(button='right')
    newx, newy = pyautogui.locateCenterOnScreen('adress_copy_symbol.png')
    newy = newy + 10
    pyautogui.moveTo(newx, newy)
    pyautogui.click()
    url_current = pyperclip.paste()
    print(os.system('cd ~/Library; wget -O \'' + download_name + '.epub\' ' +
                    url_current))

    driver.close()

def main(argv):
    # First argument is the type of books
    # Other arguments are iterated over,
    # they are the book names from the type
    book_type = sys.argv[1]
    for book in sys.argv[2:]:
        get_oreilly_book(book_type, book)

if __name__ == "__main__":
    main(sys.argv)
